# 開発スタディ


## セットアップ
### Node.js / vue-cli
Vue.js でアプリ開発をするときにとても便利な vue-cli を使えるようにします。

vue-cli を利用しなくても開発することはできますし、[公式チュートリアル](https://jp.vuejs.org/v2/guide/index.html)でも初心者は vue-cli を使うのはお勧めしないと書かれています。しかし、コードの問題点を指摘したり、修正するたびにブラウザーをリロードしてくれたり、HTML / CSS / JavaScript の結合や圧縮を自動でしてくれたりとか非常に便利ですので、今回は使います。

> Vue の他のインストール方法について、インストール ページで紹介しています。注意点として、初心者が vue-cli で始めることは推奨しません（特に、Node.js ベースのツールについてまだ詳しくない場合）。

まずは [Node.js](https://nodejs.org/ja/) から LTS をダウンロードしてインストールします。

Node.js は JavaScript の実行環境で、Vue.js のアプリ開発で利用する様々なツール群を利用するために必要です。

インストール後にコマンドプロンプトを起動して、インストールされているバージョンを確認します。

```
> node --version
v8.11.1
```

* LTS - 安定版。**こちらを推奨します**。
* 最新版 - 新しい機能が搭載されています。先進的なことをやりたい人向け。

続いて、[vue-cli](https://github.com/vuejs/vue-cli) をインストールします。コマンドプロンプトで以下のように入力します。

```
> npm install -g @vue/cli
```

`npm` コマンドは Node.js のツールやライブラリをインストールします。今回は `@vue/cli` (vue-cli) をインストールしました。

インストールが完了したら、バージョンを確認します。

```
> vue --version
3.0.0-beta.10
```

以上で Vue.js で開発する準備は完了です。


### Git for Windows
バージョン管理ツールです。

SourceTree とセットでインストールされているかもしれません。以下のコマンドを実行してバージョンが帰ってきたらインストールは不要です。

```
> git --version
git version 2.17.0-windows.1
```

インストールされていなければ、[Git for Windows](https://gitforwindows.org/) からダウンロードしてインストールしてください。


### Visual Studio Code
[Visual Studio Code](https://www.microsoft.com/ja-jp/dev/products/code-vs.aspx) は Microsoft 社が提供している無償で使えるエディターです。個人的には [WebStorm](https://www.jetbrains.com/webstorm/) がおすすめですが、こちらは有償なので、無償で使える Visual Studio Code (以下 VSCode と略します) を利用します。

[Visual Studio Code](https://www.microsoft.com/ja-jp/dev/products/code-vs.aspx) からダウンロードしてインストールしてください。

インストールが終わったら、スタートメニューから VSCode を起動します。

拡張機能をクリックして、以下の拡張機能をインストールしてください。

![](./screenshots/vscode-install-extensions.png)

* [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) - JavaScript のコードチェッカー。問題がある部分を指摘してくれます。
* [npm](https://marketplace.visualstudio.com/items?itemName=eg2.vscode-npm-script) (`npm support for Code` と書かれている方) - npm コマンドを使って開発中の Vue.js アプリを起動するのに利用します。
* [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur) - Vue.js の開発サポート用。
* [Debugger for Chrome](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome) - Visual Studio Code で JavaScript のデバッグを可能にします。


### Google Chrome
[Google Crhome](https://www.google.co.jp/chrome/index.html) はブラウザーです。

Vue.js の開発で便利な拡張機能があるので、インストールします。以下のリンクへ Chrome で飛んで、`CHROME に追加` ボタンをクリックしてください。

* [Vue.js devtools](https://chrome.google.com/webstore/detail/vuejs-devtools/nhdogjmejiglipccpnnnanhbledajbpd)

こんなかんじで、Chrome DevTools から Vue.js のコンポーネントをデバッグできます。

![](https://raw.githubusercontent.com/vuejs/vue-devtools/master/media/screenshot.png)


## チュートリアル
いっぺんにいろいろとやると混乱するので、ひとつづつやっていきます。

1. [GitLab Board を使った作業の進め方](./docs/project.md)
2. [Vue.js でアプリを作る (vue-cli)](./docs/vuecli.md)
3. [GitLab Merge Request を使ってチーム開発をする](./docs/team.md)
4. GitLab CI を使う


## メモ
* [TypeScript / JavaScript / ECMAScript](docs/typescript-ecmascript.md)
* [Router](docs/router.md)