# Vue.js でアプリを作る (vue-cli)
[vue-cli](https://github.com/vuejs/vue-cli) を使うと、本格的な Vue.js アプリ開発プロジェクトを手軽に作ることができます。

[Vue.js のチュートリアル](https://jp.vuejs.org/v2/guide/index.html)では vue-cli について以下のように書かれています。

> 注意点として、初心者が vue-cli で始めることは推奨しません（特に、Node.js ベースのツールについてまだ詳しくない場合）。


## vue-cli を使う理由
フロントエンドアプリ開発でシなければいけないことはたくさんあります。アプリの開発のためにはやらないといけないが、本当にやらないければいけない、利用者へよりいい体験を届けるというタスクにかける時間を奪い取ります。結果として、しょぼいものが出来上がります。vue-cli を使うことで、これらの問題への対処にかかる時間を減らすことができます。


**結合と圧縮と依存関係**。一般的に、開発ではソースコードの一つのファイルをなるべく小さく保つのが良いとされています。大きいと、ひとつのファイルを同時に複数人で修正することになり、変更が衝突しがちになります。

ところが、 ファイルを小さく分ければ分けるほど、ウェブアプリのパフォーマンスは低下します。例外はありますが、ウェブという仕組みは小さなたくさんのファイルを同時にダウンロードするのに向いていないからです。

さらに、`script` 要素がずらずら並んでしまい、制御できなくなる問題があります。以下の例は 5 個くらいですが、100 を超えることも珍しくありません。

```html
<script src"a.js"></script>
<script src"b.js"></script>
<script src"c.js"></script>
<script src"d.js"></script>
<script src"e.js"></script>
```

例えば、ここで `z.js` を追加するとします。`z.js` は `b.js` と `c.js` を利用していて、`e.js` から使われる予定です。どこに入れるか大変面倒ですね。

こういった面倒事を解決するために、以下のようなことをします。

* 依存関係に基づいてファイルを読みこむ
* ファイルをすべて結合してひとつのファイルにする
* ファイル内の空白文字など削れるものを削って圧縮する

**コード検査**。プログラムのバグの発見と修正は早ければ早いほど、短い時間で修正ができます。文法エラーやコードのよくないところを指摘してくれるツールが必要です。

JavaScript は他の言語に比べると罠が多く、はまりやすいのでコード検査ツールの有無は進捗に影響します。

有名どころですが、以下のコードは `undefined` と表示されます。

```javascript
function hoge() {
  return
  {
    text: hello
  }
}

console.log(hoge())
```

JavaScript には行末の `;` を自動で挿入する機能があり、結果として以下のようなコードになります。

```javascript
function hoge() {
  return; // ここにセミコロン
  {
    text: hello
  }; // ここにセミコロン
}

console.log(hoge()); // ここにセミコロン
```

**HMR (or Live Reload)**。ソースコードを修正するたびにブラウザーをリロードするのはかなりめんどくさいです。また、修正のたびにコード検査など様々な処理を実行するのもめんどくさいです。

Live Reload では、修正を検知して、再度処理をしてブラウザーを自動でリロードすることができます。複数ブラウザーでの利用もできるので、特にマルチブラウザー対応の時にもってこいです。

HMR (Hot Module Replacement) は Live Reload をもう一段便利にしたもので、ブラウザーをリロードするのではなく、変更があった部分のみを置き換えます。テキストボックスに入力されていないようが消えたりしないので、かなり便利です。

**互換性**。HTML/CSS/JavaScript は仕様は公開されていますが、すべての機能がすべてのブラウザーに実装されているとは限りません。特に、新し目の機能を使う場合、いくつかのブラウザーで動かないということになりなります。

新しい仕様の JavaScript を古い仕様の JavaScript に変換 (トランスパイル) したり、古い仕様の JavaScript にない機能をライブラリとして利用できるようにしたりします。CSS についても、(今はあまり使われていませんが) ベンダープレフィクスを使い、古いブラウザーに対応させることがあります。

いろんなブランザーで動かすために、古い仕様を使わざるを得なかったり、もしくは手動で変換するのは大変です。

**テスト*。ウェブアプリをテストするためのフレームワークを設定するのはかなり大変です。

## vue-cli を使う
**注意** ここで利用しているのは vue-cli 3 beta です。ググッて見つかる情報のほとんどは vue-cli 2 のものなので、注意してください。`vue init` でプロジェクトを作ると書かれていたら、代替 vue-cli 2 のものです。


### 簡単なコマンドプロンプトの使い方
C ドライブの `Windows` ディレクトリにカレントディレクトリを移動します。プロンプト ('>' から左側がプロンプト) にカレントディレクトリが表示されます。

```
> cd C:\Windows
C:\Windows>
``` 

エクスプローラーのアドレスバーに `cmd` と入力することで、エクスプローラーで開いているフォルダーでコマンドプロンプトを開くことができます。こちらのほうが確実です。逆に、コマンドプロンプトに `explorer .` と入力すると、エクスプローラーが開きます。

場所の間違いは気が付かないと一日を棒に振ることになるので、覚えておいてください。


### プロジェクトの作り方
エクスプローラーで作業用のフォルダーを作成し、アドレスバーに `cmd` と入力して開いたコマンドプロンプトに以下を入力してください。

```
> vue create my-project
> cd my-project
? Please pick a preset: (Use arrow keys)
❯ default (babel, eslint) 
  Manually select features 
> code .
```

Visual Studio Code が起動します。いろんなファイルが出来上がっているのだけを確認してください。(今はファイルは触らないで!)

アプリをまずは起動します。

```
> npm run serve
 DONE  Compiled successfully in 6760ms                                                                                   15:11:55

 
  App running at:
  - Local:   http://localhost:8080/
  - Network: http://192.168.30.138:8080/

  Note that the development build is not optimized.
  To create a production build, run npm run build.
```

Google Chrome で `Local` の方を開いてください。

![](./vue1.png)

`Ctrl` + `C` で一旦止めて、次に以下を入力してください。

```
> npm run build
 DONE  Compiled successfully in 7444ms                                                                                   15:15:57

  File                               Size                Gzipped

  dist/js/vendors~app.8ed9a393.js    74.14 kb            26.81 kb
  dist/js/app.0459a5ec.js            13.02 kb            8.30 kb
  dist/css/app.b5ed0656.css          0.33 kb             0.23 kb

  Images and other types of assets omitted.

 DONE  Build complete. The dist directory is ready to be deployed.
```

先程はなかった `dist` ディレクトリが作られ、圧縮や結合などの様々な処理をした成果物がこのディレクトリに出力されています。

最後に以下を入力します。

```
> npm run lint

> my-project@0.1.0 lint /home/masakura/repos/masakura/tmp/my-project
> vue-cli-service lint

 DONE  No lint errors found!
```

`npm run lint` で lint (コード検査) を実行します。最初ですので、もちろん何もありません。


### 開発してみる
開発をしてみます。

コマンドプロンプトから `npm run` と入力してもいいのですが、面倒なので Visual Studio Code 上で実行することにします。

Visual Studio Code でコマンドパレットを開いて `npm run` と入力し、 `npm run script` を選択します。`serve` とか `lint` とか表示されるので、`serve` を選びます。

その後はコマンドプロンプトで `npm run serve` とした時と同じで、ブラウザーを開いてください。

* `.gitignore` - Git の無視リスト。バージョン管理したくないファイルはここに書かれている。
* `dist` - ビルドした成果物が出力される場所。最初はありません。また、バージョン管理からは除外されています。
* `node_modules` - アプリやアプリ開発で利用する JavaScript や CSS のライブラリが格納されています。ここの中を直接修正してはいけません。
* `package.json`- アプリやアプリ開発で利用する JavaScript や CSS のライブラリが列挙されています。また、アプリの名前や様々な設定もこちらにあります。
* `package-lock.json` - `package.json` と似ていますが、このファイルは直接触ってはいけません。
* `public` - `favicon` とか `index.html` がおいてあります
* `src` - アプリのソースコードです。Vue コンポーネントや、JavaScript、画像などはこちらにあります。開発中はほとんどの場合、このディレクトリの中で作業をします。

`src` ディレクトリの中も紹介します。

* `App.vue` - アプリ全体を表示している Vue コンポーネント。
* `components/HelloWorld.vue` - ロゴ以外の部分を表示している Vue コンポーネント。

`App.vue` ファイルを見ると、`<HelloWorld msg="..."/>` というところがあり、`components/HelloWorld.vue` を利用していることがわかります。
```html
<template>
  <div id="app">
    <img src="./assets/logo.png">
    <HelloWorld msg="Welcome to Your Vue.js App"/>
  </div>
</template>
```

Chrome DevTools で `Vue` タブを開くと分かりやすいです。

![](./vue2.png)

`src/components/HelloWorld.vue` ファイルを修正します。分かりやすいですので、` For guide and recipes on how to configure` あたりを適当に書き換えてみましょう。ブラウザーのリロードがないので気が付きにくいですが、一瞬で書き換わるので注意して見ましょう!

ついでに、CSS もいじっちゃいましょう! 同じファイルの下の方にあるこれをいじっちゃいます。

```css
h3 {
  margin: 40px 0 0;
}
```

分かりやすいので背景色を変えましょう。

```css
h3 {
  margin: 40px 0 0;
  background-color: lime;
}
```

### Visual Studio Code との連携
[Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur) の設定をします。

[Visual Studio CodeでVue.jsアプリケーションの開発環境を構築する](https://qiita.com/rubytomato@github/items/b35b819671e7cbb3dff7)を参考に。

`Ctrl` + `,` でワークスペースの設定をこんな感じにする。

```json
{
    "files.eol": "\n",
    "prettier.eslintIntegration": true,
    "eslint.validate": [
        "javascript",
        "javascriptreact",
        {
            "language": "vue",
            "autoFix": true
        }
    ],
    "eslint.autoFixOnSave": false
}
```

JavaScript のエラーを即指摘してくれるようになる。

![](./eslint.png)


### Chrome Debugger と組み合わせる
デバッグを Visual Studio Code 上でできるようになります。

Visual Studio Code のデバッグから、構成を追加して launch.json ファイルを修正します。


```json
{
    // IntelliSense を使用して利用可能な属性を学べます。
    // 既存の属性の説明をホバーして表示します。
    // 詳細情報は次を確認してください: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "type": "chrome",
            "request": "launch",
            "name": "Launch Chrome",
            "url": "http://localhost:8080",
            "webRoot": "${workspaceFolder}/src",
            "breakOnLoad": true,
            "sourceMapPathOverrides": {
                "webpack:///src/*". "${webRoot}/*",
                "webpack:///*", "${webRoot}/components/*"
            }
        }
    ]
}
```

npm run serve を実行して Launch Chrome を選択すると、Chrome が起動します。Visual Studio Code でデバッグポイントを使うことができます。


## おまけ
### プロジェクトを作るときはマニュアルがいいかも
標準の vue-cli のプロジェクトはテストが入っていません。`vue create my-project` で選択することができます。

また、アプリを作るのい欠かせない Router や場合によっては使いたい vuex もありません。こちらを使いたい場合はこれらを選択してください。


### コンポーネントの作り方のコツ
最初からきちんとしたコンポーネントを書こうとすうるのはかなり大変です。ですので、大雑把に書いておいて、あとから小分けしていく感じで。

TODO 後で書く。