# JavaScript / ECMAScript / TypeScript
* JavaScript - Netscape 社が自社のブラウザーに搭載するために開発したプログラム言語。当時 Java の Sun Microsystems と仲が良かったため、Java の名前を使い、Java の影響も受けている。
* ECMAScript - JavaScript と JScript (Microsoft の実装) など、方言が増えていく JavaScript 系言語を規格化したもの。ECMA は日本でいうところの JIS みたいなもん。ECMAScript は規格、JavaScript は実装。
* TypeScript - Microsoft が開発した AltJS と呼ばれる JavaScript の代替となる言語。強い型付が特徴で、JavaScript に変換して実行される。


## JavaScript
### ECMAScript 3 (ES3)
だいぶ古い規格でほぼ見なくなった。レガシーブラウザーと言われる Internet Explorer でも ES5 対応なので、覚える必要はない。


### ECMAScript 5 (ES5)
ECMAScript 4 は迷走したので廃版になった。代わりにより現実的な ES5 が策定された。Internet Explorer が ES6 非対応なので、Internet Explorer 対応にする場合は ES5 で書く必要があるが、後述する Babel などで回避することが多い。

Vue.js のチュートリアルのほとんどはこの ES5 で書かれている。


### ECMAScript 6 (ES6)
Chrome / Firefox / Safar / Edge は大体 ES6 対応。ES6 から毎年 6 月に規格が更新されるため、年度で呼ぶことも多い。(Ex: ES2015, ES2017)

ES6 は ES5 までの JavaScript のはまりやすいところを改善している。また、より便利な機能が導入されている。

[HTML Party かごんま](http://kagoshima.html5j.org/htmlparty.2016/)の中で、Node 学園の古川さんが解説をしてくれてる。

`const` や `let` が導入され、`var` のわかりにくい挙動が改善された。

```javascript
// ES5
var text = 'hello';

function hello(flag) {
    console.log(text); // undefined
    
    if (flag) {
        var text = 'HELLO';
        console.log(text); // HELLO
    }
}

hello();
``` 

```javascript
// ES6
const text = 'hello';

function hello(flag) {
    console.log(text); // hello
    
    if (flag) {
        const text = 'HELLO';
        console.log(text); // HELLO
    }
}
```

Arrow Functions によって、より簡単に書けるようになった。`this` の挙動もより分かりやすく改定されている。

```javascript
// ES5
$('.button').on('click', function () {
   console.log('click'); 
});
```

```javascript
// ES6
$('.button').on('click', () => console.log('click'));
```

`class` 構文やよりほかの言語に近い書き方ができるように。

```javascript
// ES5
var Hello = function (text) {
    this.text = text;
};

hello.prototype.display = function () {
    console.log(this.text);  
};

var hello = new Hello('hello');
hello.display();
```

```javascript
// ES6
class Hello {
    constructor(text) {
        this.text = text;
    }
    
    display() {
        console.log(this.text);
    }
}

const hello = new Hello('hello');
hello.display();
```

上の二つは厳密には等価ではない。

```javascript
// ES5
var data = {
    text: 'hello',
    display: function () {
        console.log(this.text);
    }
};
```

```javascript
// ES6
const data = {
    text: 'hello',
    display() {
        console.log(this.text);
    }
};
```

非同期処理も簡単に書けるようになった。

```javascript
// ES5
invoke(function (error, result) {
    // 完了した後の処理 
});
```

```javascript
// ES6
invoke()
    .then(result => {
        // 完了したときの処理
    })
    .catch(error => {
        // 失敗したときの処理
    });
```

```javascript
// ES6 (ES2017)
(async () => {
    try {
        await invoke();
        // 完了したときの処理
    } catch (error) {
        // 失敗したときの処理
    }
})();
```


### トランスパイラーと Polyfill
ES6 の機能は使いたいけど、IE にも対応させなきゃいけないということはよくある。トランスパイラーと Polyfill を組み合わせることが多い。

* トランスパイラー - ES6 で書かれたコードを ES5 などの古い規格に変換する
* Polyfill - コードはそのままで追加のライブラリを読み込ませて ES6 の一部機能を ES5 で再現する方法

vue-cli で作られたプロジェクトは基本的にこのトランスパイラーは ON になっている。Polyfill は未確認。


### 古いブラウザー
サポートされているブラウザーの中で古いものは結構ある。基本、切り捨てたほうが良い。

* Internet Explorer - もう変わることはないので永遠に古いまま。
* Android 標準ブラウザー (Android 4.4 以前) - Android 4.4 以前は標準ブラウザーは Android に含まれているのですごく古い。Android 5 以降は Play Store でアップデートされるので、Chrome と同等になった。
* Firefox ESR - 通常の Firefox は二か月に一度くらいメジャーバージョンアップをする。より安定したバージョンが欲しい企業向けに、ESR というメジャーバージョンアップが一年サイクルの Firefox がある。最大で 18 ヶ月古い。
* Safari - 若干、Chrome や Firefox に比べると新機能の搭載が遅い。IE や Android 標準ブラウザーは Chrome を使ってねで逃げられるけど、iOS だけは実質 Safari のみなので対応の必要あり。


## TypeScript
強い型付が特徴。強い型付言語は Java や C# や C/C++ がそれにあたる。コードのミスをコンパイルエラーで拾いやすいのでバグが減るといわれている。JavaScript や PHP や Python は弱い型付け言語。

```javascript
// ECMAScript version
function add(left, right) {
    return left + right;
}

console.log(add(1, 2)); // 3
console.log(add('hello', 'world')); // helloworld <- 意図しない動作
```

```typescript
// TypeScript version
function add(left: number, right: number): number {
    return left + right;
}

console.log(add(1, 2)); // 3
console.log(add('hello', 'world')); // コンパイルエラー
```

ただし、弱い型付言語でも型ヒントや検査例外を使う方法もあり、デメリットというほどでもない。

```javascript
// ECMAScript version (型ヒント)

/**
 * @param {number} left
 * @param {number} right
 * @returns {number}
 */
function add(left, right) {
    return left + right;
}

console.log(add(1, 2)); // 3
console.log(add('hello', 'world')); // 警告が出る
```

```javascript
// ECMAScript version (検査例外)

function add(left, right) {
    if (isNaN(left)) throw new Error('left は数値である必要があります');
    if (isNaN(right)) throw new Error('right は数値である必要があります');
    
    return left + right;
}

console.log(add(1, 2)); // 3
console.log(add('hello', 'world')); // 実行時に例外が出る
```

最後の例は `true` や `false` を受け入れるのでちょっとおかしい。

TypeScript はメリットも多い半面、JavaScript のライブラリを呼び出しにくいデメリットもある。メジャーなライブラリは問題になることはないが、自作の JavaScript ライブラリは苦労することが多い。
